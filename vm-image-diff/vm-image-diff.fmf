---

summary: Compare two VM images, and report differences.
description: |
  Compare two VM images, and report differences.

  Accepted variables:

    CNC_IMAGE: CnC image to use for tft-admin operations.

    LEFT_COMPOSE: the first compose, "left side" of comparisons.
    LEFT_CLOUD: cloud to fetch the left compose from.
    LEFT_COMPOSE_FORMAT: format of VM image to download, .e.g qcow2, raw, etc.

    RIGHT_COMPOSE: the second compose, "right side" of comparisons.
    RIGHT_CLOUD: could to fetch the right compose from.
    RIGHT_COMPOSE_FORMAT: format of VM image to download, .e.g qcow2, raw, etc.

framework: shell
test: ./test.sh

duration: 3h

require:
  # Various build-time requirements. Once we publish a built virt-diff, we could drop these.
  - autoconf
  - autoconf-archive
  - bison
  - genisoimage
  - flex
  - ocaml
  - libguestfs-devel
  - libxml2-devel
  - ncurses-devel
  - pcre2-devel
  - jansson-devel
  - ocaml-findlib-devel
  - ocaml-libguestfs-devel
  - libtool
  - gettext-devel

  # Runtime requirements.
  - docker
  - git
  - libvirt
